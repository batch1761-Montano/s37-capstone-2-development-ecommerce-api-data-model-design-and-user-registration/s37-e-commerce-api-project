//[SECTION] Dependencies and Modules
	const express = require('express');
	const ProductController = require('../controller/products');
	const auth = require('../auth');

//[SECTION] Routing Component
	const route = express.Router();
	const multer = require('multer');
	const path = require('path');
	const Product = require('../models/Product');
	const { verify, verifyAdmin } = auth;
	
//[SECTION] Functionalities [POST]	
//[MINIMUM REQUIREMENT] Route for Create Product (Admin only)

	const storage = multer.diskStorage({
		destination: function (req, file, callback) {
			callback(null, './images/');
		},
		filename: function(request, file, callback){
			callback(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
		}
	});

	const upload = multer({
		storage: storage,
		fileFilter: (req, file, cb) => {
			if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
				cb(null, true);
			} else {
				cb(null, false);
				return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
			}
		}
	})

	route.post('/create', verify, verifyAdmin, upload.single('image'), (req, res) => {
		let orderCheck = req.body.name;
		return Product.findOne({productName: orderCheck}).then(result =>{
		console.log(req.file.path) 	
		ProductController.addProduct(req.body, req.file.path).then(result => res.send(result))
				
		})
	
	});

//[SECTION] Functionalities [RETRIEVE]	
//Retrieve all products
	route.get('/all-products', (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});


//[MINIMUM REQUIREMENT] Retrieve all ACTIVE products
	route.get('/active-products', (req, res) => {
		ProductController.getAllActiveProducts().then(result => res.send(result));
	});
//[MINIMUM REQUIREMENT] Retrieving a SINGLE product
	route.get('/:productId', (req, res) => {
		console.log(req.params.productId);
		//we can retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the URL
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});

//[SECTION] Functionalities [UPDATE]	
//[MINIMUM REQUIREMENT] Update Product information (Admin only)
	route.put('/:productId', verify, verifyAdmin, upload.single('image'), (req, res) => {
		const url = req.protocol + '://' + req.get('host')
		ProductController.updatedProduct(req.params.productId, req.body, req.file.path).then(result => res.send(result))
	});



//MINIMUM REQUIREMENT] Archiving a product (Admin Only)
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	});

//Enabling a product
	route.put('/:productId/enable', verify, verifyAdmin, (req, res) => {
		ProductController.enableProduct(req.params.productId).then(result => res.send(result));
	});

//[SECTION] Functionalities [DELETE]	
//[ADDITIONAL FEATURE] Deleting Product (Admin Only)

	route.delete('/del-product', verify, verifyAdmin, (req, res) => {

		console.log(req.params.productId);
	
		let productId = req.body.productId;
		
		ProductController.DelProduct(productId).then(resultOfDelete => res.send(resultOfDelete));
	});	

//[SECTION] Expose Route System
	module.exports = route;
