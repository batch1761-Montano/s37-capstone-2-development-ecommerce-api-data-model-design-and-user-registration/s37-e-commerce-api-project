//[SECTION] Dependencies and Modules
	const express = require('express');
	const route = express.Router();
	const OrderController = require('../controller/orders');
	const auth = require('../auth'); 

//destructure the actual function that we need to use
	const { verify, verifyAdmin } = auth;

//[MINIMUM REQUIREMENT] Non-admin User checkout (Create Order)
	
	route.post('/createorder', verify, OrderController.addOrder);
//[STRETCH GOAL]Retrieve all orders (Admin only)
	route.get('/all-orders', verify, verifyAdmin, (req, res) => {
		OrderController.getAllOrders().then(result => res.send(result));
	});

//[STRETCH GOAL]Retrieve authenticated user’s order
route.get('/getmyorders', verify, OrderController.getOrder);

//[SECTION] Functionalities [DELETE]

//[ADDITIONAL FEATURE] Deleting Order Per Verified User

	route.delete('/del-order', verify, (req, res) => {

		console.log(req.params.orderId);
	
		let orderId = req.body.orderId;
		
		OrderController.DelOrder(orderId).then(resultOfDelete => res.send(resultOfDelete));
	});	

//[SECTION] Expose Route System
	module.exports = route;
