//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controller/users');
	const auth = require('../auth'); 
	
//[SECTION] Routing Component
	const route = exp.Router();

//destructure the actual function that we need to use
	const { verify, verifyAdmin } = auth;

//[SECTION] Routes - POST

//[MINIMUM REQUIREMENT]Register New Account
	route.post('/register',(req, res) =>{
		let userData = req.body;
		controller.register(userData).then(outcome => { 
			res.send(outcome);
		});
	});
	
//[MINIMUM REQUIREMENT] User Authentication
	route.post('/login', (req, res) => {
		console.log(req.body)
		controller.loginUser(req.body).then(result => res.send(result));
	})

//[SECTION] Routes - PUT

//[STRETCH GOAL] Set user as admin (Admin only)
	route.put('/user-admin', verify, (req, res) => {
		controller.updatedUserAdmin(req.body.userId, req.body).then(result => res.send(result))
	});

//[ADDITIONAL] Set admin to user (Admin only)
	route.put('/admin-user', verify,  (req, res) => {
		console.log(req.params.userId);
		controller.AdmintoUser(req.body.userId, req.body).then(result => res.send(result))
	});

//[SECTION] Routes - GET
	route.get('/all-users', auth.verify, auth.verifyAdmin, (req, res) => {
		controller.getAllUsers().then(result => res.send(result));
	});
	
	route.get('/details', auth.verify, (req, res) => {
		controller.getAdminProfile(req.user.id).then(result => res.send(result));
	});

	//route.post('/order-products', auth.verify, controller.orders);

	route.get('/getProducts', auth.verify, controller.getProducts);
	 
//[SECTION] Expose Route System
	module.exports = route;
