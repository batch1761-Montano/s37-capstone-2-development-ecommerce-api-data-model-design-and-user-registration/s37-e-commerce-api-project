//[SECTION] Dependencies and Modules
	const jwt = require('jsonwebtoken');
	const secret = 'BreadnButterEcommerceAPI';

//Token Creation
	module.exports.createAccessToken = (user) =>{

	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

//Token Verification
	module.exports.verify = (req, res, next) => {

		console.log(req.headers.authorization)
		let token = req.headers.authorization;

		if (typeof token === "undefined"){
			return res.send ({ auth: "failed. No token"});
		} else {
			console.log(token);

			token = token.slice(7, token.length);

			jwt.verify(token, secret, function(err, decodedToken) {

				if(err) {
					return res.send({
						auth: "Failed",
						message: err.message
					})
				} else{
					console.log(decodedToken)

					req.user = decodedToken;

					next();
				}
			})

		}
	}

//Verification for an admin and will be used an a middleware

	module.exports.verifyAdmin = (req, res, next) => {

		if(req.user.isAdmin) {

			next();
		}else {
			return res.send({
				auth: "Failed",
				message: "Action Forbidden"
			});
		}
	}