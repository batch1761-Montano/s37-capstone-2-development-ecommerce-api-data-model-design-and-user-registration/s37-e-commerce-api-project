//[SECTION] Dependencies and Modules	
	const mongoose = require('mongoose');

//[SECTION] Schema / Blueprint
	const orderSchema = mongoose.Schema({
			userId:{
				type: String,
				ref: 'User',
				required: [true, 'UserID is Required']
			},
			products:[
				{
					productId:{
						type: String,
						ref: 'Product',
						required: [true, 'ProductID is Required']		
					},
					productName: {
						type: String
					 },
			
					quantity:{
						type: Number,
						default: 1
					}	
				}
			],	
			totalAmount:{
				type: Number
				
			},
			purchasedOn:{
				type: Date,
				default: new Date()
			},
	});

//[SECTION] Model
	module.exports = mongoose.model('Order', orderSchema);