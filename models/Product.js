//[SECTION] Dependencies and Modules	
	const mongoose = require('mongoose');

//[SECTION] Schema / Blueprint
	const productSchema = mongoose.Schema({
			name:{
				type: String,
				required: [true, 'Name is Required']
			},
			description:{
				type: String,
				required: [true, 'Description is Required']
			},
			price:{
				type: Number,
				required:[true, 'Price is Required']
			},
			image:{
				type: String,
				required:[true, 'Image is Required']
			},
			isActive:{
				type: Boolean,
				default: true
			},
			createdOn:{
				type: Date,
				default: new Date()
			}
	});

//[SECTION] Model
	module.exports = mongoose.model('Product', productSchema);