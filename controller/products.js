//[SECTION] Dependencies and Modules
	const mongoose = require('mongoose');
	const multer = require('multer');
	const fs = require ("fs");
	const Product = require('../models/Product');

//[SECTION] Functionalities [CREATE]
	module.exports.addProduct = (reqBody, reqFile) => {

		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			image: reqFile
			
		});

		return newProduct.save().then((product, error ) => {
			
			if(error){
				return false;
				
			} else {
				console.log(reqFile)
				return newProduct;
			}
		}).catch(error => error)
	}
//[SECTION] Functionalities [RETRIEVE]

	//Retrieve ALL products
		module.exports.getAllProducts = () => {
			return Product.find({}).then(result => {
				return result;
			}).catch(error => error)
		}

	//[MINIMUM REQUIREMENT] Retrieve all ACTIVE products
		module.exports.getAllActiveProducts = () => {
			return Product.find({ isActive: true }).then(result => {
				return result;
			}).catch(error => error)
		}

	//[MINIMUM REQUIREMENT] Retrieving a SINGLE product
		module.exports.getProduct = (reqParams) => {
			return Product.findById(reqParams).then(result => {
				return result
			}).catch(error => error)
		}

//[SECTION] Functionalities [UPDATE]

//[MINIMUM REQUIREMENT] Update Product information (Admin only)
	module.exports.updatedProduct = (productId, data, reqFile, url) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price,
			image: reqFile
		}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

	//MINIMUM REQUIREMENT] Archiving a product (Admin Only)
	module.exports.archiveProduct =(productId) => {
		let updateActiveField ={
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error) 
	}
	//[ADDITIONAL FEATURE] Enabling an Archived Product
		module.exports.enableProduct =(productId) => {
		let updateActiveField ={
			isActive: true
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error) 
	}

//[SECTION] Functionalities [DELETE]

//[ADDITIONAL FEATURE] Deleting Product (Admin Only)
	module.exports.DelProduct = (productId) => {
	
		return Product.findByIdAndRemove(productId).then((success,failed) => {
			if (success) {
				return { message: 'Your Product has been successfully removed'};
			} else {
				return { message: 'Failed to remove Product'}
			}
		}).catch(error => error)
	};