//[SECTION] Dependencies and Modules
	const Order = require('../models/Order');
	const Product = require('../models/Product');
	const User = require('../models/User');
	const mongoose = require('mongoose');

//[SECTION] Functionalities [CREATE]

//[MINIMUM REQUIREMENT] Non-admin User checkout (Create Order)
	module.exports.addOrder = async (req, res) => {
		console.log(req.body)
		if(req.user.isAdmin) {
			res.send({Warning: "You are creating an order using your Admin Access. Please use your personal account."});
		}

		const newOrder = new Order({
			userId: req.user.id,
			products: [],
			totalAmount: 0
		})

		for(let i = 0; i<req.body.length; i++) {
			let isProductAvailable = await Product.findById(req.body[i].productId)
				.then( product => {
				if(product) {
					if(product.isActive) {
						let newProduct = {
							productId: req.body[i].productId,
							productName: product.name,
							quantity: req.body[i].quantity
						}

					newOrder.totalAmount += (req.body[i].quantity * product.price)
					newOrder.products.push(newProduct)
					} else {
						res.send(`Product is not active`);
					}
				} else {
					return res.send(`Not existing product`);
				}
			})
		}

		newOrder.save().then(result => {
			return res.send(result)
		}).catch(error => res.send(error))

}


//[SECTION] Functionalities [RETRIEVE]

	//Retrieve ALL products
		module.exports.getAllOrders = () => {
			return Order.find({}).then(result => {
				return result;
			}).catch(error => error)
		}

	//[STRETCH GOAL]Retrieve all orders (Admin only)
		module.exports.getAllActiveOrders = () => {
			return Order.find({ isActive: true }).then(result => {
				return result;
			}).catch(error => error)
		}

	//[STRETCH GOAL]Retrieve authenticated user’s order
		module.exports.getOrder = (req,res) => {
			Order.find({ 'userId' : req.user.id })
			.then(result =>
				{
					res.send(result)
			})
			.catch(error => err.message)
		}
		

//[SECTION] Functionalities [DELETE]
//[ADDITIONAL FEATURE] Deleting Order Per Verified User
	module.exports.DelOrder = (orderId) => {
	
		return Order.findByIdAndRemove(orderId).then((success,failed) => {
			if (success) {
				return { message: 'Your Order has been successfully removed'};
			} else {
				return { message: 'Failed to remove Order'}
			}
		

		});
	};
	
//[SECTION] Add Item to Order
module.exports.addItemtoCart = () => {


}