//[SECTION] Dependencies and Modules
  const User = require('../models/User'); 
  const Product = require('../models/Product');
  const Order = require('../models/Order');
  const bcrypt = require('bcrypt');
  const dotenv = require ('dotenv');
  const auth = require('../auth.js');

//[SECTION] Environment Variables Setup
	dotenv.config();
	const alat = Number(process.env.SALT);

//[SECTION] Functionalities [CREATE]

//[MINIMUM REQUIREMENT]Register New Account
	module.exports.register = (userData) => {
		let email = userData.email;
		let passW = userData.password;

		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(passW, alat)
		});

		return newUser.save().then((user, err) =>{

			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register account'};
			};
		});
	};

//[MINIMUM REQUIREMENT] User Authentication
module.exports.loginUser = (data) => {
		
		return User.findOne({ email: data.email }).then(result => {
				//User does not exist

				if(result == null) {
						return false;
				} else {

						const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

						if(isPasswordCorrect){
								return { accessToken: auth.createAccessToken(result.toObject()) }
						}else {
							
							return false;
						}

				}
		})
}

//[SECTION] Functionalities [RETRIEVE] the user details

module.exports.getAdminProfile = (data) => {
	return User.findById(data).then(result => {

		result.password = '';
		return result;
	})
}
/* //[SECTION] Functionalities [POST]
module.exports.orders = async (req, res) => {

		if (req.user.isAdmin) {
				return res.send ({ message: "Action Forbidden"})
		}

		let isOrdersUpdated = await User.findById(req.user.id).then( user => {

			let newProduct = {
				productId: req.body.productId
		
			}

			user.products.push(newProduct);

			return user.save().then(user => true).catch(err => err.message)

			if (isOrdersUpdated !== true) {
						return res.send({ message: isOrdersUpdated })
			} 

	});
	
		let isOrderUpdated = await Order.findById(req.body.productId).then(order => {

					let userOrders = {
						userId: req.body.userId
					}

					order.products.push(userOrders);

					//save the course document
					return order.save().then(order => true).catch(err => err.message)
					if (isOrderUpdated !== true) {
							return res.send (({ message: isOrderUpdated }))
					}
		})

		if(isOrdersUpdated && isOrderUpdated) {
				return res.send({ message: "Purchased Successfully" })
		}
} */

module.exports.getProducts = (req, res) => {
	User.findById(req.user.id).then(result => res.send(result.products)).catch(err => res.send(err));
}

module.exports.getAllUsers = () => {
			return User.find({}).then(result => {
				return result;
			}).catch(error => error)
		}
		
//[STRETCH GOAL] Set user as admin (Admin only)
	module.exports.AdmintoUser = (userId) => {
		let AdmintoUser = {
			isAdmin: false
		}

	return User.findByIdAndUpdate(userId, AdmintoUser).then((user, error) => {
			if(error){
				return false;
			} else {
				return AdmintoUser;
			}
		}).catch(error => error)
	}

//[SECTION] Functionalities [UPDATE]

//[STRETCH GOAL] Set user as admin (Admin only)
	module.exports.updatedUserAdmin = (userId) => {
		let updatedUserAdmin = {
			isAdmin: true
		}
		
	return User.findByIdAndUpdate(userId, updatedUserAdmin).then((user, error) => {
			if(error){
				return false;
			} else {
				return updatedUserAdmin;
			}
		}).catch(error => error)
	}
//[SECTION] Functionalities [DELETE]